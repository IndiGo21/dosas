#include "mainwindow.h"
#include "windows.h"
#include <QApplication>
#include "QProcess"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DWORD pid = (DWORD) atoi(argv[1]); // берем pid процесса
    bool isAttached = DebugActiveProcess(pid); // подключаемся к нему как отладчик
    if(!isAttached){ // проверка удалось ли подключиться
        DWORD lastError = GetLastError();
        qDebug() << lastError;
        return lastError;
    }
    DEBUG_EVENT DbgEvent;
    while(1){  // бесконечный цикл ожидания
        WaitForDebugEvent(&DbgEvent, INFINITE);
        ContinueDebugEvent(DbgEvent.dwProcessId, DbgEvent.dwThreadId, DBG_CONTINUE);
    }
}
